class Fixnum


  def initialize(num)
    @num = num
  end

  NUMBERS = {
    1 => "one",
    2 => "two",
    3 => "three",
    4 => "four",
    5 => "five",
    6 => "six",
    7 => "seven",
    8 => "eight",
    9 => "nine",
    10 => "ten",
    11 => "eleven",
    12 => "twelve",
    13 => "thirteen",
    14 => "fourteen",
    15 => "fifteen",
    16 => "sixteen",
    17 => "seventeen",
    18 => "eighteen",
    19 => "nineteen",
    20 => "twenty",
    30 => "thirty",
    40 => "forty",
    50 => "fifty",
    60 => "sixty",
    70 => "seventy",
    80 => "eighty",
    90 => "ninety",
    100 => "hundred",
    1000 => "thousand",
    1000000 => "million",
    1000000000 => "billion",
    1000000000000 => "trillion"
  }

  def in_words
    if self == 0
      "zero"
    elsif self < 100
      tens(self)
    elsif self < 1000
      hundreds(self)
    elsif self < 1000000
      thousands(self)
    elsif self < 1000000000
      millions(self)
    elsif self < 1000000000000
      billions(self)
    elsif self < 1000000000000000
      trillions(self)
    end
  end

  def tens(num)
    if num < 20
      NUMBERS[num]
    else
      num % 10 != 0 ?
        NUMBERS[num - num % 10] + " " + NUMBERS[num % 10] :
        NUMBERS[num]
    end
  end

  def hundreds(num)
    num_str = ""
    if num / 100 != 0
      num_str = NUMBERS[num / 100] + " " + NUMBERS[100]
    end
    num % 100 == 0 ?
      num_str.strip :
      (num_str + " " + tens(num % 100)).strip

  end

  def thousands(num)
    num_str = ""
    if num / 1000 != 0
      if NUMBERS[num / 1000] != nil
        num_str = NUMBERS[num / 1000] + " " + NUMBERS[1000]
      else
        num_str = hundreds(num / 1000) + " " + NUMBERS[1000]
      end\

    end
    num % 1000 == 0 ?
      num_str.strip :
      (num_str + " " + hundreds(num % 1000)).strip
  end


  def millions(num)
    num_str = ""
    if num / 1000000 != 0
      if NUMBERS[num / 1000000] != nil
        num_str = NUMBERS[num / 1000000] + " " + NUMBERS[1000000]
      else
        num_str = hundreds(num / 1000000) + " " + NUMBERS[1000000]
      end
    end
    num % 1000000 == 0 ?
      num_str.strip :
      (num_str + " " + thousands(num % 1000000)).strip
  end

  def billions(num)
    num_str = ""
    if num / 1000000000 != 0
      if NUMBERS[num / 1000000000] != nil
        num_str = NUMBERS[num / 1000000000] + " " + NUMBERS[1000000000]
      else
        num_str = hundreds(num / 1000000000) + " " + NUMBERS[1000000000]
      end
    end
    num % 1000000000 == 0 ?
      num_str.strip :
      (num_str + " " + millions(num % 1000000000)).strip
  end

  def trillions(num)
    num_str = ""
    if num / 1000000000000 != 0
      if NUMBERS[num / 1000000000000] != nil
        num_str = NUMBERS[num / 1000000000000] + " " + NUMBERS[1000000000000]
      else
        num_str = hundreds(num / 1000000000000) + " " + NUMBERS[1000000000]
      end
    end
    num % 1000000000000 == 0 ?
      num_str.strip :
      (num_str + " " + billions(num % 1000000000000)).strip
  end
end
